""" Implementation of rejection-acceptance sampling strategy for random diffusion eigen-value
    triplets with uniform distributions over specified FA and MD ranges"""

__all__ = ["exponentially_sample_simplex", "uniformly_sample_simplex", "MCRejectionSampler",
           "reject_box_constraint", "non_uniform_tensor_samples"]

from typing import Tuple, Optional
import warnings
import sys

import tensorflow as tf
import numpy as np

import cdtipy.tensor_metrics
from scipy.ndimage import gaussian_filter1d


@tf.function
def exponentially_sample_simplex(trace: float, intervals: np.ndarray, n_samples: int, **kwargs):
    """ Uniformly samples eigenvalues in given intervals and projects the vector onto the plane with

    .. math::
        \Sigma _i ev_i = 1

    Afterwards the vector of eigenvalues is scaled to the plane representing all combinations
    with sum of eigenvalues equal to 'trace' specified in the argument.

    :param trace:
    :param intervals:
    :param n_samples:
    :param kwargs:
    :return:
    """
    seed = kwargs.get('seed', None)

    sample = tf.random.uniform((n_samples, 3), 0, 1., dtype=tf.float32, seed=seed)
    factors = intervals[:, 1] - intervals[:, 0]
    offsets = intervals[:, 0]
    evs = sample * factors[tf.newaxis, :] + offsets[tf.newaxis, :]
    evs = evs * trace / tf.reduce_sum(evs, axis=1, keepdims=True)
    return evs


@tf.function
def uniformly_sample_simplex(trace: float, intervals: np.ndarray, n_samples: int, **kwargs):
    """ Uses algorithm described here:
     https://cs.stackexchange.com/questions/3227/uniform-sampling-from-a-simplex
    to get a sample from a uniform distribution on a simplex

    :param trace: (float) Determines simplex plane
    :param intervals: (n_evs, 2) rows: eigenvalue index cols: (lower bound: upper bound)
    :param n_samples: (int) number of samples
    :param kwargs: - seed: int seed for each eigenvalue batch-sample
    :return: (n_samples, n_evs)
    """
    seed = kwargs.get('seed', None)
    random_samples = tf.random.uniform(shape=(n_samples, 2), minval=0., maxval=1.,
                                       dtype=tf.float32, seed=seed)
    value_list = tf.concat([tf.zeros((n_samples, 1), dtype=tf.float32),
                            random_samples, tf.ones((n_samples, 1), dtype=tf.float32)], axis=1)
    sorted_values = tf.sort(value_list, axis=1)
    unit_evs = sorted_values[:, 1:] - sorted_values[:, :-1]
    return unit_evs * trace


class MCRejectionSampler(tf.Module):
    def __init__(self, trace_support, eigenvalue_supports: np.ndarray, fa_support: np.ndarray,
                 sampling_method: callable = exponentially_sample_simplex,
                 burn_in_samples: int = None,
                 burn_in_values: Optional[Tuple[np.ndarray, np.ndarray, np.ndarray]] = None):
        """

        :param trace_support:
        :param eigenvalue_supports:
        :param fa_support:
        :param burn_in_samples: (value_range, mc_decision_boundaries, n_bins)
        :param sampling_method: defaults to "exponentially_sample_simplex", other methods are not
                                  guaranteed to produce a flat distribution over FA
        """
        super(MCRejectionSampler, self).__init__()
        self.eigenvalue_supports = tf.constant(eigenvalue_supports, dtype=tf.float32)
        self.fa_supports = tf.constant(fa_support, dtype=tf.float32)
        self._sampling_method = sampling_method
        self.trace_support = trace_support
        if burn_in_values is None:
            if burn_in_samples < 1e5:
                warnings.warn(
                    f'Using only {burn_in_samples} < {1e5} samples to burn in '
                    f'distribution will likely fail!')
            self._value_range, self.mc_decision_boundaries, self._n_bins = self._burn_in(
                burn_in_samples)
        else:
            self._value_range, self.mc_decision_boundaries, self._n_bins = burn_in_values

    def _burn_in(self, burn_in_samples: int, batch_size=10000):
        """ Samples #burn_in_samples eigen-value triples to obtain a proximal representation
        of the distribution over FA. To uniformize it, Monte Carlo weights are
        calculated from the samples """

        tf.print('Burn in FA distribution: ', output_stream=sys.stdout)
        print(f"{0} / {burn_in_samples // batch_size}", end="")

        burn_in_fa = []
        for i in range(int(np.ceil(burn_in_samples / batch_size))):
            random_trace_values = tf.random.uniform((batch_size,), minval=self.trace_support[0],
                                                    maxval=self.trace_support[1], dtype=tf.float32)
            ev_samples = tf.map_fn(lambda t: self._sampling_method(t, self.eigenvalue_supports, 1),
                                   random_trace_values)
            ev_samples = tf.squeeze(ev_samples)
            box_constraint_ev_samples = reject_box_constraint(ev_samples, self.eigenvalue_supports,
                                                              self.fa_supports)
            burn_in_fa = cdtipy.tensor_metrics.fractional_anisotropy(box_constraint_ev_samples)
            print(f"\r {i + 1} / {burn_in_samples // batch_size}", end="")

        burn_in_fa = tf.concat(burn_in_fa, axis=0)
        value_range = tf.stack([tf.reduce_min(burn_in_fa), tf.reduce_max(burn_in_fa)], axis=0)
        n_bins = tf.cast(tf.round((value_range[1] - value_range[0]) / 0.01), tf.int32)
        hist = tf.histogram_fixed_width(burn_in_fa, value_range, nbins=n_bins)
        probabilities = hist / tf.reduce_sum(hist)
        probabilities = gaussian_filter1d(probabilities, 3)

        mc_decision_boundaries = probabilities * tf.cast(n_bins, tf.float32) / 2  # p(FA)
        return tf.cast(value_range, tf.float32), tf.cast(mc_decision_boundaries, tf.float32), n_bins

    @tf.function
    def reject(self, proposed_ev_samples):
        box_constraint_rejection = reject_box_constraint(proposed_ev_samples,
                                                         self.eigenvalue_supports,
                                                         self.fa_supports)

        md = tf.reduce_mean(box_constraint_rejection, axis=1, keepdims=True)
        numerator = tf.reduce_sum((box_constraint_rejection - md) ** 2, axis=1)
        denominator = tf.reduce_sum(tf.pow(box_constraint_rejection, 2), axis=1)
        fa = tf.sqrt(3. / 2. * numerator / denominator)
        bin_indices = tf.histogram_fixed_width_bins(fa, self._value_range, nbins=self._n_bins)

        # Actual MC-rejection
        mc_sample = tf.random.uniform(shape=tf.shape(box_constraint_rejection)[0:1],
                                      minval=0., maxval=1.)
        comparing_values = tf.gather(self.mc_decision_boundaries, indices=bin_indices)
        accepted_samples = tf.gather(box_constraint_rejection,
                                     tf.where(mc_sample > comparing_values), axis=0)[:, 0]
        n_rejected_samples = tf.shape(proposed_ev_samples)[0] - tf.shape(accepted_samples)[0]

        # Pad with zeros to ensure shape
        accepted_samples = tf.concat((accepted_samples, tf.zeros((n_rejected_samples, 3),
                                                                 dtype=tf.float32)), axis=0)
        return tf.ensure_shape(accepted_samples, proposed_ev_samples.shape)

    def sample(self, trace: float, n_samples: int):
        return self._sampling_method(trace, n_samples)

    def get_samples(self, n_samples: int, verbose: bool = True):
        """ Generates n samples of sorted eigenvalues with uniform distribution over MD and
        FA.

        :param n_samples:
        :param verbose:
        :return:
        """
        n_samples = tf.constant(n_samples, dtype=tf.int32)
        accepted_eigenvalues = tf.TensorArray(dtype=tf.float32, size=n_samples, element_shape=(3,))

        for i in tf.range(n_samples, dtype=tf.int32):
            single_ev_sample = self.get_single_sample()
            accepted_eigenvalues = accepted_eigenvalues.write(i, single_ev_sample)
            if verbose and (i + 1) % 400 == 0:
                tf.print(f"\rCurrently accepted samples:", i + 1, " / ", n_samples,
                         output_stream=sys.stdout, end='')
        return accepted_eigenvalues.stack()

    @tf.function
    def get_single_sample(self, n_parallel_samples: int = 4):
        """

        :param n_parallel_samples:
        :return: (3, ) sorted in descending order
        """
        random_trace = tf.random.uniform(shape=(1,), minval=self.trace_support[0],
                                         maxval=self.trace_support[1])
        samples_found = tf.constant(0, dtype=tf.int32)
        acceptable_samples = tf.zeros(shape=(n_parallel_samples, 3), dtype=tf.float32)

        while tf.math.less(samples_found, tf.constant(1, dtype=tf.int32)):
            # Sample simplex with given sampling method
            proposed_ev_samples = tf.cast(self._sampling_method(random_trace,
                                                                self.eigenvalue_supports,
                                                                n_parallel_samples), tf.float32)

            # Check which samples need to be rejected
            acceptable_samples = self.reject(proposed_ev_samples)
            samples_found = tf.size(tf.where(tf.reduce_sum(acceptable_samples, axis=1) > 0.))

        return tf.sort(acceptable_samples[0], direction="DESCENDING")

    def save(self, path: str):
        np.savez(path, bins=self._n_bins, fa_supports=self.fa_supports.numpy(),
                 eigenvalue_supports=self.eigenvalue_supports,
                 trace_support=self.trace_support, value_range=self._value_range,
                 mc_decision_boundaries=self.mc_decision_boundaries)

    @classmethod
    def from_savepath(cls, path: str, sampling_method: callable = exponentially_sample_simplex):
        load_params = np.load(path)
        instance = cls(trace_support=load_params['trace_support'],
                       eigenvalue_supports=load_params['eigenvalue_supports'],
                       fa_support=load_params['fa_supports'], sampling_method=sampling_method,
                       burn_in_values=(load_params['value_range'],
                                       load_params['mc_decision_boundaries'],
                                       load_params['bins']))
        instance._n_bins = load_params['bins']
        return instance


def non_uniform_tensor_samples(n_samples: int, min_values: Tuple[float, float, float],
                               max_values: Tuple[float, float, float], mean_trace: float,
                               seed: Optional[int] = None,
                               check_results: Optional[bool] = True) -> tf.Tensor:
    """ Creates positive definite (3x3) tensors that are randomly oriented and have a

    :param n_samples: (int)
    :param min_values: Lower bounds for sampling interval of the eigenvalue scaling [0, 1]
    :param max_values: Upper bounds for sampling interval of the eigenvalue scaling [0, 1]
    :param mean_trace: Mean value of the resulting MD (normally distributed as the sum of 3
                            uniform random variables)
    :param seed: Value to set the seed of the sampling process to reproduce a dataset.
    :param check_results: if True checks returned tensors for positive definiteness.
                            raises AssertionError if
                            eigenvalues <= are detected
    :return:
    """
    with tf.device('CPU:0'):
        samples = tf.random.normal((n_samples, 3, 3), mean=0., stddev=1.,
                                   dtype=tf.float32, seed=seed)
        symmetric_tensors = tf.einsum('nij, njk -> nik', samples, samples)

        # sample from uniform interval
        scaling = tf.random.uniform((n_samples, 3), 0, 1., dtype=tf.float32, seed=seed)

        # Transform to different uniform intervals
        intervals = (tf.constant(max_values, shape=(1, 3)) - tf.constant(min_values, shape=(1, 3)))
        offsets = tf.constant(min_values, shape=(1, 3))
        eigen_value_scaling = ((scaling * intervals + offsets) * mean_trace /
                               tf.reduce_sum(offsets + intervals / 2))
        _, eig_vecs = tf.linalg.eigh(symmetric_tensors)

        # transposing of second rotation matrix is done in indices of einsum
        return_tensors = tf.einsum('nij, njk, nlk -> nil', eig_vecs,
                                   tf.linalg.diag(eigen_value_scaling), eig_vecs)

        if check_results:
            assert (all(ev > 0 for ev in (tf.linalg.eigvalsh(return_tensors).numpy()).flatten()))
        return return_tensors


@tf.function
def reject_box_constraint(proposed_ev_samples, eigenvalue_supports, fa_support):
    """ Gathers the subset of eigenvalue combinations, that lie within the box-contraints
     for ev and FA specified by the arguments. For the eigenvalues only one interval is allowed,
     for FA multiple intervals can be given (Any-condition).

    :param proposed_ev_samples: (#samples, #evs)
    :param eigenvalue_supports: (#evs, 2)
    :param fa_support: (#intervals, 2)
    :return:
    """
    evs_ok = _is_in_evs_support(proposed_ev_samples, eigenvalue_supports)
    fa_ok = _is_in_fa_support(proposed_ev_samples, fa_support)
    conditionals = tf.reduce_all(tf.stack((evs_ok, fa_ok), axis=0), axis=0)
    acceptable_samples = tf.gather(proposed_ev_samples, tf.where(conditionals), axis=0)
    return acceptable_samples[:, 0, :]


@tf.function
def _is_in_evs_support(evs: tf.Tensor, intervals: tf.Tensor):
    """ Subbtract upper bound and lower bound, if product of these differences is -
        negative --> lies within support
    is_inside_support
    :param evs: (#samples, #evs)
    :param intervals: (#evs, 2)
    :return: bool (#samples, )
    """
    temp = evs[:, :, tf.newaxis] - intervals[tf.newaxis, ...]
    temp = tf.reduce_prod(temp, axis=-1, keepdims=False) < 0.
    all_evs_in_support = tf.reduce_all(temp, axis=1, keepdims=False)
    return all_evs_in_support


@tf.function
def _is_in_fa_support(evs: tf.Tensor, intervals: tf.Tensor):
    """

    :param evs: (n_samples, 3)
    :param intervals: (n_intervals, 2): n times (lower bound, upper bound)
    :return: boolean (#samples, )
    """
    md = tf.reduce_mean(evs, axis=1, keepdims=True)
    numerator = tf.reduce_sum((evs - md) ** 2, axis=1)
    denominator = tf.reduce_sum(tf.pow(evs, 2), axis=1)
    fa = tf.sqrt(3. / 2. * numerator / denominator)

    temp = fa[:, tf.newaxis, tf.newaxis] - intervals[tf.newaxis, ...]  # n_samples x n_intervals x 2
    is_within_at_least_one_interval = tf.reduce_any(tf.reduce_prod(temp, axis=2) < 0., axis=1)
    return is_within_at_least_one_interval


if __name__ == "__main__":
    with tf.device("CPU:0"):
        mc_sampler = MCRejectionSampler([3, 6],
                                        np.array([[0., 3.], [0., 3.], [0., 3.]], dtype=np.float32),
                                        np.array([0.2, 0.7]), burn_in_samples=25000,
                                        sampling_method=exponentially_sample_simplex)
        mc_sampler.get_samples(20000, verbose=True)
