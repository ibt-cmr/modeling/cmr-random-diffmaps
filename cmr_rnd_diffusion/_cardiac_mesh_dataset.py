import os
import warnings
from typing import List, Union, Tuple, Sequence, Iterable

import pyvista
import numpy as np
from tqdm.notebook import tqdm
from pint import Quantity

import cdtipy

import cmr_rnd_diffusion
import cmr_rnd_diffusion.mesh_utils
import cmr_rnd_diffusion.vtk_utils
import cmr_rnd_diffusion.diff3d
import vtk


class BaseDataset(object):
    """ """
    #: Mesh containing the displacements for each time-point
    mesh: Union[pyvista.UnstructuredGrid, pyvista.DataSet]
    #: Time points corresponding to the mesh states given by the list of files in ms
    timing: np.ndarray

    def __init__(self, mesh: Union[pyvista.UnstructuredGrid, pyvista.DataSet],
                 timing_ms: np.ndarray):
        """
        :param mesh: pyvista mesh containing the field-array 'displacement_{time}ms' for each
                    time specified in timing_ms
        :param timing_ms: array containing the times corresponding to the stored displacements
        """
        self.mesh = mesh
        self.timing = timing_ms

    def add_data_per_time(self, scalars: np.array, name: str):
        """

        :param scalars: (t, #p, ...)
        :param name:
        :return:
        """
        for timing, value in zip(self.timing, scalars):
            self.mesh[f"{name}_{timing}ms"] = value

    def transform_vectors(self, time_stamps: np.ndarray,
                          vector_names: List[str],
                          reference_time: Quantity = None,
                          rotation_only: bool = True):
        """ Evalutes the deformation matrix for ref-timing -> time_stamps and applies the
        transformation to the vector arrays of specified names in the mesh. Results are stored
        as vector field in self.mesh.

        :param time_stamps:
        :param vector_names: Names of properties that are transformed
        :param reference_time: Time that correspond to the definition of the vectors
        :param rotation_only: If true the transformation includes a normalizatios, thus results in a
                rotation only.
        :return:
        """
        reference_positions = self.get_positions_at_time(reference_time)

        deformation_mesh = pyvista.UnstructuredGrid(self.mesh.cells_dict, reference_positions)

        # Apply transformation for all time steps
        for ts in tqdm(time_stamps, desc="Computing deformation matrices", leave=False):
            # Compute the deformation matrix on the mesh serving as transformation
            for idx, direction in enumerate("xyz"):
                deformation_mesh[f"disp{direction}"] = self.mesh[f"displacements_{ts}ms"][:, idx]
                deformation_mesh = deformation_mesh.compute_derivative(scalars=f"disp{direction}",
                                                                       gradient=f"d{direction}_dxyz")
                deformation_mesh[f"d{direction}_dxyz"][
                    np.logical_not(np.isfinite(deformation_mesh[f"d{direction}_dxyz"]))] = 0

            nabla_u = np.stack([deformation_mesh["dx_dxyz"], deformation_mesh["dy_dxyz"],
                                deformation_mesh["dz_dxyz"]], axis=1)
            F = nabla_u + np.eye(3, 3).reshape(1, 3, 3)

            # Apply all transformations and store result in self.mesh
            for vec_name in vector_names:
                self.mesh[f"{vec_name}_{ts}ms"] = np.einsum("nji, nj -> ni",
                                                            F, self.mesh[vec_name])
                if rotation_only:
                    norm = np.linalg.norm(self.mesh[f"{vec_name}_{ts}ms"], axis=-1,
                                          keepdims=True)
                    self.mesh[f"{vec_name}_{ts}ms"] /= norm

    def render_input_animation(self, filename: str, plotter: pyvista.Plotter = None,
                               scalar: str = None, vector: str = None,
                               start: Quantity = Quantity(0., "ms"),
                               end: Quantity = None, mesh_kwargs: dict = None,
                               vector_kwargs: dict = {"mag": 1e-3},
                               text_kwargs: dict = dict(position='upper_right', 
                                                        color='white', shadow=False,
                                                        font_size=26)):
        """ Saves a gif animation using the original input mesh and displacements.

        :param filename:
        :param plotter:
        :param scalar:
        :param vector:
        :param start:
        :param end:
        :param mesh_kwargs:
        :param vector_kwargs:
        """
        plotting_mesh = self.mesh.copy()

        index_start = np.searchsorted(self.timing, start.m_as("ms"))
        reference_position = np.array(plotting_mesh.points)
        initial_position = (reference_position +
                            plotting_mesh[f"displacements_{self.timing[index_start]}ms"])

        if scalar is not None:
            plotting_mesh.set_active_scalars(f"{scalar}_{self.timing[index_start]}ms")

        if mesh_kwargs is None:
            mesh_kwargs = dict()

        if end is None:
            index_end = -1
        else:
            index_end = np.searchsorted(self.timing, end.m_as("ms"), side="right")

        if plotter is None:
            plotter = pyvista.Plotter(off_screen=True)

        plotting_mesh.points = initial_position
        plotter.add_mesh(plotting_mesh, **mesh_kwargs)

        plotter.open_gif(f"{filename}.gif")
        pbar = tqdm(self.timing[index_start:index_end],
                    desc=f"Rendering Mesh-Animation from {self.timing[index_start]:1.3f}"
                         f" ms to {self.timing[index_end]:1.3f} ms")
        for timing in pbar:
            text_actor = plotter.add_text(f"{timing: 1.2f} ms", **text_kwargs)
            plotter.update_coordinates(reference_position +
                                       plotting_mesh[f"displacements_{timing}ms"],
                                       mesh=plotting_mesh, render=False)
            if scalar is not None:
                pbar.set_postfix_str(f"Update scalar with: {scalar}_{self.timing[index_start]}ms")
                plotter.update_scalars(plotting_mesh[f"{scalar}_{timing}ms"],
                                       mesh=plotting_mesh,
                                       render=False)
            if vector is not None:
                arrow_actor = plotter.add_arrows(
                    cent=reference_position + plotting_mesh[f"displacements_{timing}ms"],
                    direction=plotting_mesh[f"{vector}_{timing}ms"],
                    **vector_kwargs
                )

            plotter.render()
            plotter.write_frame()
            plotter.remove_actor(text_actor)
            if vector is not None:
                plotter.remove_actor(arrow_actor)

        plotter.close()

    def get_positions_at_time(self, time: Quantity):
        """ Returns mesh nodes at the closest match to time argument

        :param time: Quantity
        :return:
        """
        if time is not None:
            index_start = np.searchsorted(self.timing, time.m_as("ms"))
            reference_position = (self.mesh.points +
                                  self.mesh[f"displacements_{self.timing[index_start]}ms"])
        else:
            reference_position = np.array(self.mesh.points)
        return reference_position

    def probe_reference(self, reference_mesh: pyvista.UnstructuredGrid,
                        field_names: List[str], reference_time: Quantity = None):
        """ Probes the reference mesh with the points of self.mesh at reference time and stores
        the probed fields matching the given names into self.mesh.

        :param reference_mesh:
        :param field_names:
        :param reference_time:
        :return:
        """
        reference_positions = self.get_positions_at_time(reference_time)
        probing_mesh = pyvista.UnstructuredGrid(self.mesh.cells_dict, reference_positions)
       
        probing_mesh = reference_mesh.probe(probing_mesh)
        for name in field_names:
            self.mesh[name] = probing_mesh[name]

    def get_field(self, field_name: str,  timing_indices: Iterable[int] = None) \
            -> (Quantity, np.ndarray):
        """ Gets the specified field at all given time-indices and returns a stacked array

        :param field_name:
        :param timing_indices:
        :return:
        """
        if timing_indices is not None:
            times = Quantity([self.timing[t] for t in timing_indices], "ms")
        else:
            times = Quantity(self.timing, "ms")
        field_arr = np.stack([self.mesh[f"{field_name}_{t}ms"] for t in times.m], axis=0)
        return times, field_arr

    def select_slice(self, slice_position: Quantity, slice_normal: np.ndarray,
                     slice_thickness: Quantity, reference_time: Quantity) \
            -> pyvista.UnstructuredGrid:
        """
        :param slice_position: (3, ) vector denoting the slice position
        :param slice_normal: (3, ) vector denoting the normal vector of the slice
                    (is normalized internally)
        :param slice_thickness: Thickness of the selected slice (along the slice_normal)
        :param reference_time:
        :return: mesh containing only the cell being inside the specified slice
        """
        time_idx = np.searchsorted(self.timing, reference_time.m_as("ms"), side="right")
        all_points = self.mesh.cell_centers().points + \
                     self.mesh.ptc()[f"displacements_{self.timing[time_idx]}ms"]
        slice_normal = slice_normal / np.linalg.norm(slice_normal)
        slice_position = slice_position.m_as("m")
        d1 = np.einsum("ni, i", all_points - slice_position[np.newaxis, :], slice_normal)
        in_slice = np.abs(d1) < slice_thickness.m_as("m") / 2
        mesh_copy = self.mesh.copy()
        current_slice = mesh_copy.remove_cells(np.where(np.logical_not(in_slice)),
                                               inplace=True)
        current_slice.reference_time = reference_time
        return current_slice

    def get_trajectories(self, start: Quantity, end: Quantity) -> Tuple[Quantity, Quantity]:
        """ Returns available times and positions between specified start-end

        :param start:
        :param end:
        :return: time, positions (N, t, 3)
        """
        time_idx_start = np.searchsorted(self.timing, start.m_as("ms"), side="right")
        time_idx_end = np.searchsorted(self.timing, end.m_as("ms"), side="right")
        time = Quantity(self.timing[time_idx_start:time_idx_end], "ms")
        positions = Quantity(np.stack([self.mesh.points + self.mesh[f"displacements_{t}ms"]
                                       for t in time.m], axis=1), "m")
        return time, positions


class CardiacMeshDataset(BaseDataset):
    """ Cardiac mesh model assuming the continuous node-ordering:

            [apex, n_points_per_ring(1st slice, 1st shell), n_points_per_ring(2st slice, 1st shell),
            ..., n_points_per_ring(1st slice, 2nd shell)]

        shell --> radial
        slice --> longitudinal
    """
    #: (n_shells, n_points_per_ring, n_slices)
    mesh_numbers: Tuple[int, int, int]

    def __init__(self, mesh: Union[pyvista.UnstructuredGrid, pyvista.DataSet],
                 mesh_numbers: (int, int, int),
                 timing_ms: np.ndarray):
        """
        :param mesh: pyvista mesh containing the field-array 'displacement_{time}ms' for each
                    time specified in timing_ms
        :param mesh_numbers: (shells, circ, long)
        :param timing_ms: array containing the times corresponding to the stored displacements
        """
        super().__init__(mesh, timing_ms)
        self.mesh_numbers = mesh_numbers

    @classmethod
    def from_list_of_arrays(cls, initial_mesh: pyvista.DataSet, mesh_numbers: (int, int, int),
                            list_of_files: List, timing: Quantity, time_precision_ms: int = 1):
        """ Loads a mesh and its displacements from a list of numpy files each containing the
        positional vectors for all points in initial_mesh.

        :raises: FileNotFoundError - if not all files in given list of files exist

        :param initial_mesh: Initial mesh configuration
        :param mesh_numbers: (shells, circ, long)
        :param list_of_files: List of .npy containing the position of all mesh nodes per time
        :param timing:
        :param time_precision_ms: number of decimals used in the field name and timepoints
                                        (e.g. displacements_0.01ms)
        """
        # Make sure all file exist before starting to load all of them
        files_exist = [os.path.exists(f) for f in list_of_files]
        if not all(files_exist):
            n_not_found = len(list_of_files) - sum(files_exist)
            non_existent_files = "\n\t".join([f for f, exists in zip(list_of_files, files_exist)
                                              if not exists])
            raise FileNotFoundError(f"Could not find {n_not_found} files:\n\t {non_existent_files}")

        timing_ms = np.around(timing.m_as("ms"), decimals=time_precision_ms)
        initial_mesh[f"displacements_{timing_ms[0]}ms"] = np.zeros_like(initial_mesh.points)
        for file, time in tqdm(zip(list_of_files[1:], timing_ms[1:]), desc="Loading Files... ",
                               total=len(list_of_files)):
            temp_rvecs = np.load(file)
            initial_mesh[f"displacements_{time}ms"] = temp_rvecs - initial_mesh.points
        return cls(initial_mesh, timing_ms=timing_ms, mesh_numbers=mesh_numbers)

    @classmethod
    def from_list_of_meshes(cls, list_of_files: List, timing: Quantity, mesh_numbers: (int, int, int),
                            field_key: str = "displacement", time_precision_ms: int = 1):
        """ Loads

        :raises: FileNotFoundError - if not all files in given list of files exist

        :param list_of_files: List of .vtk or .vti files each containing a mesh in which the
                              displacement vectors for each node is stored under the name of
                              specified by the argument field_key
        :param timing:
        :param mesh_numbers: (shells, circ, long)
        :param field_key: key to extract
        :param time_precision_ms: number of decimals used in the field name and timepoints
                                        (e.g. displacements_0.01ms)
        """

        # Make sure all file exist before starting to load all of them
        files_exist = [os.path.exists(f) for f in list_of_files]
        if not all(files_exist):
            n_not_found = len(list_of_files) - sum(files_exist)
            non_existent_files = "\n\t".join([f for f, exists in zip(list_of_files, files_exist)
                                              if not exists])
            raise FileNotFoundError(f"Could not find {n_not_found} files:\n\t {non_existent_files}")

        # Load initial vtk file and add all displacements to the mesh as field array
        mesh = pyvista.read(list_of_files[0])
        timing_ms = np.around(timing.m_as("ms"), decimals=time_precision_ms)
        mesh[f"displacements_{timing_ms[0]}ms"] = np.zeros_like(mesh.points)
        for file, time in tqdm(zip(list_of_files[1:], timing_ms[1:]), desc="Loading Files... ",
                               total=len(list_of_files)-1):
            temp_mesh = pyvista.read(file)
            mesh[f"displacements_{time}ms"] = temp_mesh[field_key]
        return cls(mesh, timing_ms=timing_ms, mesh_numbers=mesh_numbers)

    @classmethod
    def from_single_vtk(cls, file_name: str, mesh_numbers: (int, int, int),
                        time_precision_ms: int = 3):
        """ Assumes, the displacements to be saved as defined in the other class methods

        :param file_name:
        :param mesh_numbers: (shells, circ, long)
        :param time_precision_ms: number of decimals used in the field name and timepoints
                                        (e.g. displacements_0.01ms)
        :return:
        """
        mesh = pyvista.read(file_name)
        timings = [float(t.replace("displacements_", "").replace("ms", "")) for t
                   in mesh.array_names if "displacements" in t]
        timings.sort()
        timings = np.around(np.array(timings), decimals=time_precision_ms)
        return cls(mesh, timing_ms=timings, mesh_numbers=mesh_numbers)

    def refine(self, longitudinal_factor: int, circumferential_factor: int, radial_factor: int)\
            -> 'CardiacMeshDataset':
        """ Refines the cardiac mesh model assuming the continous node-ordering:

            [apex, n_points_per_ring(1st slice, 1st shell), n_points_per_ring(2st slice, 1st shell),
            ..., n_points_per_ring(1st slice, 2nd shell)]

        shell --> radial
        slice --> longitudinal

        :param longitudinal_factor:
        :param circumferential_factor:
        :param radial_factor:
        :return:
        """
        n_shells, n_points_per_ring, n_slices = self.mesh_numbers
        original_ref_points = self.mesh.points + self.mesh[f"displacements_{self.timing[0]}ms"]
        refined_ref_positions, new_dimensions = cmr_rnd_diffusion.mesh_utils.interpolate_mesh(
            original_ref_points, points_per_ring=n_points_per_ring,
            n_shells=n_shells, n_slices=n_slices,
            interp_factor_c=circumferential_factor,
            interp_factor_l=longitudinal_factor,
            interp_factor_r=radial_factor)
        connectivity = cmr_rnd_diffusion.mesh_utils.evaluate_connectivity(*new_dimensions)
        new_mesh = pyvista.UnstructuredGrid(self.mesh.cells_dict, refined_ref_positions)
        refined_ref_positions = np.array(new_mesh.points)

        for idx, timing in tqdm(enumerate(self.timing), total=len(self.timing),
                                desc=f"Refining mesh by factors: l={longitudinal_factor}, "
                                     f"c={circumferential_factor}, r={radial_factor}"):
            original_points = self.mesh.points + self.mesh[f"displacements_{self.timing[idx]}ms"]
            refined_positions, new_mesh_numbers = cmr_rnd_diffusion.mesh_utils.interpolate_mesh(
                original_points, points_per_ring=n_points_per_ring,
                n_shells=n_shells, n_slices=n_slices,
                interp_factor_c=circumferential_factor,
                interp_factor_l=longitudinal_factor,
                interp_factor_r=radial_factor)
            new_mesh[f"displacements_{timing}ms"] = refined_positions - refined_ref_positions
        new_dataset = CardiacMeshDataset(new_mesh, new_mesh_numbers, self.timing)
        return new_dataset

    def compute_local_basis(self, time_stamp: Quantity,
                            keys: Tuple[str, str, str] = ("e_t", "e_c", "e_l")):
        """

        :param time_stamp:
        :param keys:
        :return:
        """
        positions = self.get_positions_at_time(time_stamp)
        local_basis = cmr_rnd_diffusion.diff3d.compute_local_basis(positions,
                                                     [self.mesh_numbers[i] for i in (0, 2, 1)])
        local_basis = [local_basis[k] for k in ("e_r", "e_c", "e_l")]
        for k, v in zip(keys, local_basis):
            self.mesh[k] = v

    def compute_cylinder_coords(self, time_stamp: Quantity):
        """

        :param time_stamp:
        :return:
        """
        positions = self.get_positions_at_time(time_stamp)
        cyl_coords = cmr_rnd_diffusion.mesh_utils.evaluate_cylinder_coordinates(
                        positions, n_shells=self.mesh_numbers[0],
                        points_per_shell=1 + self.mesh_numbers[1] * self.mesh_numbers[2]
                        )
        self.mesh["transmural_depth"] = cyl_coords[..., 0]
        self.mesh["polar_angle"] = cyl_coords[..., 1]
        self.mesh["z-pos"] = cyl_coords[..., 2]


class DiffusionDataset(BaseDataset):
    def angles_from_ref(self, reference_mesh: pyvista.UnstructuredGrid,
                        time_stamp: Quantity):
        """

        :param reference_mesh:
        :param time_stamp: time-stamp for self.mesh at which it matches the motion state of the
                            reference_mesh
        :return:
        """
        if not all(k in reference_mesh.array_names for k in ["ha", "e2a"]):
            raise KeyError("Necessary keys [ha, e2a] for helix angle and sheetlet angle "
                           f"not contained in reference mesh: ({reference_mesh.array_names})")

        original_pos = self.mesh.points
        self.mesh.points = self.get_positions_at_time(time_stamp)
        self.probe_reference(reference_mesh, ["ha", "e2a", "e_t", "e_c", "e_l"])
        self.mesh["haref"] = self.mesh["ha"]
        self.mesh["e2aref"] = self.mesh["e2a"]
        self.mesh.points = original_pos

    def random_angles(self, helix_kw=dict(angle_range=(75, -75)),
                      e2a_kw=dict(down_sampling_factor=15 ** 2.,
                                  seed_fraction=0.15, prob_high=0.6, high_angle_value=90., low_angle_value=1.,
                                  kernel_width=0.15, distance_weights=(0.7, 1., 10.),
                                  verbose=True)):
        """

        :param helix_kw:
        :param e2a_kw:
        :return:
        """
        ha = cmr_rnd_diffusion.diff3d.generate_ha(self.mesh["transmural_depth"], **helix_kw)
        cyl_coords = np.stack([self.mesh[k] for k in ("transmural_depth", "polar_angle", "z-pos")],
                              axis=1)
        e2a = cmr_rnd_diffusion.diff3d.generate_e2a(cyl_coords, **e2a_kw)
        self.mesh["haref"] = ha
        self.mesh["e2aref"] = e2a

    def sample_tensors(self, eigen_value_pool: np.array,
                       interpolation_kwargs: dict = None):
        """

        :param eigen_value_pool:
        :param interpolation_kwargs:
        :return:
        """
        if interpolation_kwargs is None:
            interpolation_kwargs = dict(seed_fraction=0.2, distance_weights=(1., 0.8, 15.),
                                        kernel_variance=0.01)
        local_basis = np.stack([self.mesh[k] for k in ("e_t", "e_c", "e_l")], axis=-1).astype(np.float32)
        flat_eigen_basis = cmr_rnd_diffusion.diff3d.create_eigenbasis(
                                local_basis, self.mesh["haref"].astype(np.float32),
                                self.mesh["e2aref"].astype(np.float32))
        cyl_coords = np.stack([self.mesh[k] for k in ("transmural_depth", "polar_angle", "z-pos")],
                              axis=-1)
        tensors = cmr_rnd_diffusion.diff3d.generate_tensors(eigen_value_pool, flat_eigen_basis,
                                                            cyl_coords, **interpolation_kwargs)
        tensors *= 1e-3
        if np.sum(np.logical_not(np.isfinite(tensors))) > 0:
            warnings.warn("Non finite values in tensor field")

        self.mesh["tensorsref"] = np.reshape(tensors, [-1, 9])

        metrics = cdtipy.tensor_metrics.metric_set(tensors)
        self.mesh["evalsref"] = metrics["evals"].reshape(-1, 3)
        self.mesh["fibersref"] = metrics["evecs"][..., 0].reshape(-1, 3)
        self.mesh["sheetsref"] = metrics["evecs"][..., 1].reshape(-1, 3)
        self.mesh["evec3ref"] = metrics["evecs"][..., 2].reshape(-1, 3)

    def rotate_tensors(self, time_stamps: Quantity, ref_time: Quantity,
                       rotate_local_basis: bool = True):
        """

        :param time_stamps:
        :param ref_time:
        :param rotate_local_basis:
        :return:
        """
        ref_position = self.get_positions_at_time(ref_time)
        deformation_mesh = pyvista.UnstructuredGrid(self.mesh.cells_dict, ref_position)

        for ts in tqdm(time_stamps.m_as("ms"), desc="Computing deformation matrices", leave=False):
            current_positions = self.get_positions_at_time(Quantity(ts, "ms"))
            displacement = current_positions - ref_position
            for idx, direction in enumerate("xyz"):
                deformation_mesh[f"disp{direction}"] = displacement[..., idx]
                deformation_mesh = deformation_mesh.compute_derivative(scalars=f"disp{direction}",
                                                                       gradient=f"d{direction}_dxyz")
                deformation_mesh[f"d{direction}_dxyz"][
                    np.logical_not(np.isfinite(deformation_mesh[f"d{direction}_dxyz"]))] = 0

            nabla_u = np.stack([deformation_mesh["dx_dxyz"], deformation_mesh["dy_dxyz"],
                                deformation_mesh["dz_dxyz"]], axis=1)
            F = nabla_u + np.eye(3, 3).reshape(1, 3, 3)

            rotated_eigenvecs = np.stack([np.einsum('nji, nj -> ni', F, self.mesh[k])
                                          for k in ["fibersref", "sheetsref", "evec3ref"]], axis=1)
            rotated_eigenvecs /= np.linalg.norm(rotated_eigenvecs, axis=2, keepdims=True)
            rotated_inverse = np.linalg.inv(rotated_eigenvecs)

            diagonal_diffusion_tensors = np.einsum("...ij, ...j -> ...ij", np.eye(3, 3),
                                                   self.mesh["evalsref"])
            rotated_tensors = np.einsum("nij, njk, nkl -> nil", rotated_inverse,
                                        diagonal_diffusion_tensors, rotated_eigenvecs)
            self.mesh[f"diffusion_tensors_{ts}ms"] = rotated_tensors.reshape(-1, 9)
            if rotate_local_basis:
                errot = np.einsum("nji, nj -> ni", F, self.mesh["e_t"])
                ecrot = np.einsum("nji, nj -> ni", F, self.mesh["e_c"])
                elrot = np.einsum("nji, nj -> ni", F, self.mesh["e_l"])
                self.mesh[f"local_basis_{ts}ms"] = np.stack([errot, ecrot, elrot],
                                                            axis=2).reshape(-1, 9)

    def animate_tensors(self, animation_timestamps: Sequence[float],
                        file_name: str,
                        plotter: pyvista.Plotter = None,
                        indices: np.ndarray = None,
                        plot_histogram_chart: bool = True,
                        chart_pos_loc: Tuple[Tuple, Tuple] = ((0.65, 0.26), (0.225, 0.715)), 
                        plot_glyphs: bool = True):
        """ Saves an animation of the transformed diffusion tensors for given time-stamps """

        if plotter is None:
            plotter = pyvista.Plotter(off_screen=True)

        plotter.open_gif(file_name)

        if indices is None:
            indices = np.arange(0, self.mesh.points.shape[0])

        for idx, ts in tqdm(enumerate(animation_timestamps),
                            total=len(animation_timestamps),
                            desc="Rendering Tensor animation"):
            # Compute cdti metric/angulation for tensors at time-stamp
            tens = self.mesh[f"diffusion_tensors_{ts}ms"].astype(np.float32).reshape(-1, 3, 3)[
                indices]
            local_basis = self.mesh[f"local_basis_{ts}ms"].reshape(-1, 3, 3)[indices]

            plot_mesh = pyvista.PolyData(self.mesh.points[indices])
            plot_mesh.points += self.mesh[f"displacements_{ts}ms"][indices]

            metrics = cdtipy.tensor_metrics.metric_set(tens)
            metrics["HA"] = cdtipy.angulation.helix_angle(metrics["evecs"][0, ..., 0],
                                                          local_basis).numpy()
            metrics["E2A"] = np.abs(cdtipy.angulation.sheetlet_angle(metrics["evecs"][0, ..., 0],
                                                                     metrics["evecs"][0, ..., 1],
                                                                     local_basis))
            plot_mesh["fibers"] = metrics["evecs"][0, ..., 0]
            plot_mesh["sheets"] = metrics["evecs"][0, ..., 1]
            plot_mesh["HA"] = metrics["HA"]
            plot_mesh["E2A"] = metrics["E2A"]

            # Plot histogram chart
            if plot_histogram_chart:
                if idx == 0:
                    f, axes = cdtipy.plotting.scalars.hist_summary(metrics, angles=metrics)
                    f.set_size_inches((18, 4))
                    axes[1][0].set_ylabel("")
                    f.subplots_adjust(bottom=0.22, top=0.9, left=0.01, right=0.95)

                    chart = pyvista.ChartMPL(f, size=chart_pos_loc[0], loc=chart_pos_loc[1])
                    chart.border_color = (1, 1, 1, 0)
                    chart.background_color = (1, 1, 1, 0)
                    chart_actor = plotter.add_chart(chart)
                else:
                    [a.clear() for a in f.axes]
                    _ = cdtipy.plotting.scalars.hist_summary(metrics, angles=metrics,
                                                             f_axes=(f, axes))
                    axes[1][0].set_ylabel("")
                    f.subplots_adjust(bottom=0.22, top=0.9, left=0.01, right=0.95)

            # Plot glyphs or scalars
            if plot_glyphs:
                artists = self._plot_ha_e2a(plotter, plot_mesh)
            else:
                artists = self._plot_scalars(plotter, plot_mesh)

            # Write frame and reset plotter
            plotter.render()
            plotter.write_frame()
            [plotter.remove_actor(_) for _ in artists]
        plotter.close()

    @staticmethod
    def _plot_ha_e2a(plotter: pyvista.Plotter, plot_mesh: pyvista.PolyData):
        """ Plots cylinders according to fibers and sheets in the plot_mesh dataset """
        geom = pyvista.Cylinder(radius=0.0003, height=0.005)
        # Plot helix angle glyphs
        glyphs = plot_mesh.glyph(orient="fibers", scale="HA", factor=0.025, geom=geom)
        art = plotter.add_mesh(glyphs, cmap="rainbow", clim=[-70, 70],
                               scalar_bar_args=dict(height=0.5, width=0.02, vertical=True,
                                                    position_x=0.025, position_y=0.05,
                                                    title_font_size=16, label_font_size=12))
        # Plot glyphs
        pl2 = plot_mesh.copy()
        pl2.points += np.array([0.05, 0.05, 0.])
        glyphs2 = pl2.glyph(orient="sheets", scale="E2A", factor=0.025, geom=geom)

        art2 = plotter.add_mesh(glyphs2, cmap="seismic", clim=[0, 90],
                                scalar_bar_args=dict(
                                    height=0.5, width=0.02, vertical=True, position_x=0.055,
                                    position_y=0.05, title_font_size=16, label_font_size=12)
                                )
        return [art, art2]

    @staticmethod
    def _plot_scalars(plotter: pyvista.Plotter, plot_mesh: pyvista.PolyData):
        art1 = plotter.add_mesh(plot_mesh, scalars="HA", cmap="jet", clim=[-70, 70],
                                scalar_bar_args=dict(height=0.5, width=0.02, vertical=True,
                                                     position_x=0.025, position_y=0.05,
                                                     title_font_size=16, label_font_size=12))
        pl2 = plot_mesh.copy()
        pl2.points += np.array([0.065, 0.065, 0.])
        art2 = plotter.add_mesh(pl2, scalars="E2A", cmap="seismic", clim=[0, 90],
                                scalar_bar_args=dict(height=0.5, width=0.02, vertical=True,
                                                     position_x=0.055, position_y=0.05,
                                                     title_font_size=16, label_font_size=12))
        return [art1, art2]

