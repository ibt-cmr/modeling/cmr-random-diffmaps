"""Utility to work with LV-meshes defined by Buoso ()"""

__all__ = ["interpolate_mesh", "evaluate_connectivity", "evaluate_cellsize", "select_slice"]

import numpy as np
from typing import Tuple
from scipy import interpolate
from tqdm import tqdm


def interpolate_mesh(mesh_points: np.ndarray, points_per_ring: int = 40, n_shells: int = 4,
                     n_slices: int = 30, interp_factor_c: int = 1, interp_factor_l: int = 1,
                     interp_factor_r: int = 1) -> (np.ndarray, Tuple[int, int, int]):
    """ Function to linear interpolate points of LV meshes in circular, longitudinal and
     radial direction. Indexing of the points is assumed to adhere to the following structure:
             0th element -> Apex
             1:points_per_ring+1 -> inner most shell most apical slice.
             Repeated for n_slices.
             Repeated for n_shells.

    :param mesh_points: np.ndarray - (-1, 3) Flattened array of 3D positional vectors of
                        mesh points.
    :param points_per_ring: (int) number of points per ring (circumferential direction)
    :param n_shells: (int) number of shells (radial direction)
    :param n_slices: (int) number of slices (longitudinal direction)
    :param interp_factor_c: (int) interpolation multiplier for circumferential direction
    :param interp_factor_l: (int) interpolation multiplier for longitudinal direction
    :param interp_factor_r: (int) interpolation multiplier for radial direction
    :returns: np.ndarray - (n_shell_new, 1 + (n_theta_new*n_slices_new), 3),
                (n_theta_new, n_slices_new, n_shells_new)
    """

    points_per_shell = 1 + points_per_ring * n_slices
    all_points_per_shell = np.stack(
        [mesh_points[i * points_per_shell:(i + 1) * points_per_shell] for i in range(n_shells)])

    points_per_shell_interp_c, new_n_theta = _interp_circ(all_points_per_shell, interp_factor_c,
                                                          points_per_ring, n_slices)

    points_per_shell_interp_cl, new_n_slices = _interp_long(points_per_shell_interp_c,
                                                            interp_factor_l, new_n_theta)
    points_per_shell_interp_clr, new_n_shells = _interp_radial(
        np.stack(points_per_shell_interp_cl, axis=0), interp_factor_r)
    return points_per_shell_interp_clr.reshape(-1, 3), (new_n_theta, new_n_slices, new_n_shells)


def _interp_circ(mesh_layers: np.ndarray, factor: int = 4, points_per_ring: int = 40,
                 n_slices: int = 30) -> (np.ndarray, int):
    """ Interpolates mesh in circumferential direction. As the last interval on a ring in larger than
    the rest, an additional point is inserted there, even if the interpolation factor is 1. This
    results in new_points_per_ring = points_per_ring * factor + 1.

    :param mesh_layers: (n_shells, points_per_ring * n_slices + 1, 3)
    :param factor: (int) interpolation multiplier
    :param points_per_ring: (int) number of points per ring for input argument mesh_layers
    :param n_slices: (int) number of slices for input argument mesh_layers
    :returns: (n_shells, new_points_per_ring * n_slices + 1, 3), new_points_per_ring
    """
    fail_cout = []
    factor_last = factor + 1
    new_points_per_ring = points_per_ring * factor + 1
    result = []
    phi_new = np.arange(0, new_points_per_ring) * 2. * np.pi / new_points_per_ring
    for shell_index, points_of_shell in enumerate(mesh_layers):
        # Apex point is saved at location 0 -> no interpolation
        points_per_slice = np.stack(
            [points_of_shell[1 + i * points_per_ring:1 + (i + 1) * points_per_ring] for i in
             range(n_slices)])

        # c_points_interp = np.zeros((points_per_slice.shape[0], new_points_per_ring, 3))
        # for slice_idx, r_pos in enumerate(points_per_slice):
        #     r_pos_com = np.mean(r_pos, axis=0)
        #     phi = np.angle(r_pos[..., 0] - r_pos_com[0] + 1j *
        #                    (r_pos[..., 1] - r_pos_com[1])) + np.pi
        #     try:
        #         fx = interpolate.CubicSpline(np.concatenate([phi, phi[0:1]+2*np.pi], axis=0),
        #                                      np.concatenate([r_pos, r_pos[0:1]], axis=0),
        #                                      bc_type='periodic')
        #     except:
        #         fail_cout.append(slice_idx)
        #         idx = np.argsort(phi)
        #         phi = phi[idx]
        #         r_pos = r_pos[idx]
        #         fx = interpolate.CubicSpline(np.concatenate([phi, phi[0:1] + 2 * np.pi], axis=0),
        #                                      np.concatenate([r_pos, r_pos[0:1]], axis=0),
        #                                      bc_type='periodic')
        #     c_points_interp[slice_idx] = fx(phi_new)
        # print(shell_index, fail_cout)

        c_points_interp = np.stack([(points_per_slice[:, 1:] - points_per_slice[:, :-1])
                                    * i / factor + points_per_slice[:, :-1] for i in range(factor)],
                                   axis=-2)
        c_points_interp = c_points_interp.reshape(n_slices, -1, 3)

        last_interval = np.stack([(points_per_slice[:, 0:1] - points_per_slice[:, -1:])
                                  * i / (factor_last) + points_per_slice[:, -1:] for i in
                                  range(factor_last)], axis=-2)
        last_interval = last_interval.reshape(n_slices, -1, 3)

        c_points_interp = np.concatenate([c_points_interp, last_interval], axis=1).reshape(-1, 3)
        result.append(np.concatenate([points_of_shell[0:1],
                                      c_points_interp.reshape(-1, 3)], axis=0))

    return np.stack(result, axis=0), new_points_per_ring


def _interp_long(mesh_layers: np.ndarray, factor: int = 4, points_per_ring: int = 40,
                 n_slices: int = 30) -> (np.ndarray, int):
    """ Interpolates mesh along longitudinal direction. The number of slices excludes the apex.
    The interpolation includes the interval between the apex and the first slice, therefore the
    resulting number of slices after interpolation evaluates to n_slices_new = n_slices * factor.

    :param mesh_layers: (n_shells, points_per_ring * n_slices + 1, 3)
    :param factor: (int) interpolation multiplier
    :param points_per_ring: (int) number of points per ring for input argument mesh_layers
    :param n_slices: (int) number of slices for input argument mesh_layers
    :returns: (n_shells, points_per_ring * n_slices_new + 1, 3)
    """
    new_n_slices = n_slices * factor
    result = []
    if factor == 1:
        return mesh_layers, n_slices

    for shell_index, points_of_shell in enumerate(mesh_layers):
        # Unstack longitudinal slices (each having points_per_ring elements)
        points_per_slice = points_of_shell[1:].reshape(n_slices, points_per_ring, 3)

        # Concatenate broadcasted singularity (apex) to insert slices there as well
        apex_to_first_interp = np.stack([(points_per_slice[0] - points_per_slice[1:2]) *
                                         i / factor + points_per_slice[1:2] for i in
                                         range(1, factor)], axis=1)
        apex_to_first_interp = apex_to_first_interp[:, ::-1].reshape(-1, points_per_ring, 3)

        # interpolate, most basal layer is dropped
        points_interp = np.stack([(points_per_slice[1:] - points_per_slice[:-1]) *
                                  i / factor + points_per_slice[:-1] for i in range(factor)],
                                 axis=1)
        points_interp = points_interp.reshape(-1, points_per_ring, 3)

        points_interp = np.concatenate([apex_to_first_interp, points_interp], axis=0).reshape(-1, 3)

        # strip apex and append basal
        points_interp = np.concatenate([points_of_shell[0:1], points_interp, points_per_slice[-1]],
                                       axis=0)
        result.append(points_interp)

    return np.stack(result, axis=0), new_n_slices


def _interp_radial(mesh_layers: np.ndarray, factor: int = 4) -> (np.ndarray, int):
    """ Interpolates mesh along radial direction. Interpolation is performed in the n_shells-1
    intervals, therefore the resulting number of shells is n_shells_new = (n_shells - 1) * factor + 1

    interpolation includes the interval between the apex and the first slice, therefore the resulting
    number of slices after interpolation evaluates to n_slices_new = n_slices * factor.

    :param mesh_layers: (n_shells, points_per_ring * n_slices + 1, 3)
    :param factor: (int) interpolation multiplier
    :returns: (n_shells, points_per_ring * n_slices_new + 1, 3)
    """
    n_shells, points_per_shell = mesh_layers.shape[0:2]
    new_n_shells = (n_shells - 1) * factor + 1
    shells_without_apex = mesh_layers[:, 1:, :]
    x_ref = np.arange(0., n_shells, 1.)
    x_new = np.concatenate([np.arange(i, i+1, 1/factor) for i in range(n_shells-1)])
    x_new = np.concatenate([x_new, [n_shells-1, ]], axis=0)
    shells_interp = np.zeros((new_n_shells, points_per_shell-1, 3))
    for cl_idx in range(points_per_shell-1):
        r_pos = shells_without_apex[:, cl_idx]
        fx = interpolate.InterpolatedUnivariateSpline(x_ref, r_pos[..., 0], k=3)
        fy = interpolate.InterpolatedUnivariateSpline(x_ref, r_pos[..., 1], k=3)
        fz = interpolate.InterpolatedUnivariateSpline(x_ref, r_pos[..., 2], k=3)
        shells_interp[:, cl_idx] = np.stack([fx(x_new), fy(x_new), fz(x_new)], axis=-1)


    # shells_interp = np.stack([(shells_without_apex[1:] - shells_without_apex[0:-1])
    #                           * i / factor + shells_without_apex[0:-1] for i in range(factor)],
    #                          axis=1)
    # shells_interp = shells_interp.reshape(-1, points_per_shell - 1, 3)
    #
    # shells_interp = np.concatenate([shells_interp, shells_without_apex[-1:]], axis=0)
    shells_interp_with_apex = np.concatenate(
        [np.tile(mesh_layers[0:1, 0:1, :], [shells_interp.shape[0], 1, 1]),
         shells_interp], axis=1)

    return shells_interp_with_apex, new_n_shells


def evaluate_connectivity(n_theta: int, n_radial:int, n_layers: int):
    """Generates connectivity for tetrahedral UnstructuredGrid in vtk format
    Inputs:
        - n_theta  : number of points along circumferential contour (n points for each ring)
        - n_radial : number of points along longitudinal direction
        - n_layers : number of points along radial direction

    Outputs:
        - connectivity : connectivity array where row i contains the vertex id of points defining tetrahedron with index i

    Dr. Buoso Stefano (2020)
    email: buoso@biomed.ee.ethz

    """
    offset_layer0 = n_radial * n_theta + 1

    connectivity = []
    offset = 0

    for k in range(n_layers - 1):
        for j in range(n_theta - 1):
            connectivity.append(
                [offset, j + 1 + offset, j + 2 + offset, j + 2 + offset + offset_layer0])
            connectivity.append([offset, j + 1 + offset, j + 2 + offset + offset_layer0,
                                 j + 1 + offset + offset_layer0])
            connectivity.append(
                [offset, j + 1 + offset + offset_layer0, j + 2 + offset + offset_layer0,
                 offset + offset_layer0])
        connectivity.append([offset, j + 2 + offset, 1 + offset, 1 + offset + offset_layer0])
        connectivity.append(
            [offset, j + 2 + offset, 1 + offset + offset_layer0, j + 2 + offset + offset_layer0])
        connectivity.append([offset, j + 2 + offset + offset_layer0, 1 + offset + offset_layer0,
                             offset + offset_layer0])

        for i in range(0, n_radial - 1):
            for j in range(n_theta - 1):
                connectivity.append([j + 1 + offset, j + 2 + offset + n_theta, j + 2 + offset,
                                     j + 2 + offset + n_theta + offset_layer0])
                connectivity.append(
                    [j + 1 + offset, j + 2 + offset + n_theta + offset_layer0, j + 2 + offset,
                     j + 2 + offset + offset_layer0])
                connectivity.append([j + 1 + offset, j + 2 + offset + n_theta + offset_layer0,
                                     j + 2 + offset + offset_layer0,
                                     j + 1 + offset + offset_layer0])

                connectivity.append(
                    [j + 1 + offset, j + 1 + offset + n_theta, j + 2 + offset + n_theta,
                     j + 2 + offset + n_theta + offset_layer0])
                connectivity.append([j + 1 + offset, j + 1 + offset + n_theta,
                                     j + 2 + offset + n_theta + offset_layer0,
                                     j + 1 + offset + n_theta + offset_layer0])
                connectivity.append([j + 1 + offset, j + 1 + offset + n_theta + offset_layer0,
                                     j + 2 + offset + n_theta + offset_layer0,
                                     j + 1 + offset + offset_layer0])
            connectivity.append([j + 2 + offset, j + 3 + offset, j + 3 + offset - n_theta,
                                 j + 3 + offset + offset_layer0])
            connectivity.append(
                [j + 2 + offset, j + 3 + offset + offset_layer0, j + 3 + offset - n_theta,
                 j + 3 + offset - n_theta + offset_layer0])
            connectivity.append([j + 2 + offset, j + 3 + offset + offset_layer0,
                                 j + 3 + offset - n_theta + offset_layer0,
                                 j + 2 + offset + offset_layer0])
            connectivity.append([j + 2 + offset, j + 2 + offset + n_theta, j + 3 + offset,
                                 j + 3 + offset + offset_layer0])
            connectivity.append(
                [j + 2 + offset, j + 2 + offset + n_theta, j + 3 + offset + offset_layer0,
                 j + 2 + offset + n_theta + offset_layer0])
            connectivity.append([j + 2 + offset, j + 2 + offset + n_theta + offset_layer0,
                                 j + 3 + offset + offset_layer0, j + 2 + offset + offset_layer0])

            offset += n_theta
        offset = (k + 1) * offset_layer0

    return np.array(connectivity)


def evaluate_cellsize(points: np.ndarray, connectivity: np.ndarray) \
        -> (np.ndarray, np.ndarray, np.ndarray):
    """ Evaluates cell centers and cell volumes for the tetra-mesh given as points and connectivty.

    :param points: (#points, 3)
    :param connectivity: (#cells, 4)
    :returns: cell_center (#cells, 3), cell_volumes (#cells), point_weights (#points)
    """
    coordinates = points[connectivity]
    cell_centers = np.mean(coordinates, axis=1)
    tetra_edge_vectors = coordinates[:, 1:] - coordinates[:, 0:1]
    cell_volumes = np.abs(
        np.einsum("ni,ni->n", np.cross(tetra_edge_vectors[:, 0], tetra_edge_vectors[:, 1]),
                  tetra_edge_vectors[:, 2])) / 6
    cell_volumes_tiled = np.tile(cell_volumes[:, np.newaxis], [1, 4]).reshape(-1) / 4
    point_weights = np.zeros(points.shape[0])
    point_weights[connectivity.reshape(-1)] += np.tile(cell_volumes[:, np.newaxis],
                                                       [1, 4]).reshape(-1) / 4

    point_weights = (np.abs(point_weights)) / np.mean(np.abs(point_weights))
    cell_volumes = (np.abs(cell_volumes)) / np.mean(np.abs(cell_volumes))

    return cell_centers, cell_volumes, point_weights


def select_slice(all_points: np.ndarray, slice_normal: np.ndarray,
                 slice_position: np.ndarray, slice_thickness: float):
    """
    :param all_points: [-1, 3]
    :param slice_normal: (x, y, z)
    :param slice_position: (x, y, z)
    :param slice_thickness: float
    """
    slice_normal = slice_normal / np.linalg.norm(slice_normal)
    d1 = np.einsum("ni, i", all_points - slice_position[np.newaxis, :], slice_normal)
    in_slice = np.abs(d1) < slice_thickness / 2
    return all_points[np.where(in_slice)], in_slice


def evaluate_cylinder_coordinates(positional_vectors: np.ndarray, n_shells: int,
                                  points_per_shell: int):
    """
    :param positional_vectors: (-1, 3)
    :param n_shells: (int)
    :param points_per_shell: (int)
    :return: np.ndarray (-1, 3) [radial, angle, relative z]
    """
    outer_shell = positional_vectors[1:points_per_shell, 0:2]
    inner_shell = positional_vectors[1 + (n_shells - 1) * points_per_shell:, 0:2]

    r_vec = positional_vectors.reshape(n_shells, points_per_shell, 3)
    transmural_position = np.linalg.norm(r_vec[:, 1:, 0:2] - inner_shell, axis=-1)
    transmural_position /= np.linalg.norm(outer_shell - inner_shell, axis=-1)

    angle = np.angle(r_vec[:, 1:, 0] + 1j * r_vec[:, 1:, 1])

    normalized_z_pos = r_vec[..., 2] / np.abs(r_vec[..., 2]).max()

    cylinder_coordinates = np.stack([transmural_position, angle,
                                     normalized_z_pos[:, 1:]], axis=-1)
    apex = np.concatenate([np.zeros_like(r_vec[:, 0:1, 0:2]),
                           normalized_z_pos[:, 0:1, np.newaxis]], axis=-1)
    cylinder_coordinates = np.concatenate([apex, cylinder_coordinates], axis=1)
    return cylinder_coordinates.reshape(-1, 3)


def create_lv_mesh(n_theta: int = 40, n_slices: int = 30, n_shells: int = 4):
    """ Creates a tetrahedal unstructured mesh following the logic of LV meshes from array of points.

    :param all_points: (-1, 3) array of point coordinates
    :param n_theta: number of points along circumferential contour (n points for each ring)
    :param n_slices: number of points along longitudinal direction
    :param n_shells: number of points along radial direction
    :return:
    """

    points_per_shell = n_slices * n_theta + 1
    connectivity = []

    r, l = points_per_shell, n_theta
    for shell_index in range(n_shells - 1):
        j = shell_index * points_per_shell

        # add cells including the apex as vertex (2 cells)
        for i, i_1 in zip(range(1, n_theta + 1), list(range(2, n_theta + 1)) + [1, ]):
            connectivity += [(j, j + i, j + i_1, j + i + r), (j, j + i_1, j + i + r, j + i_1 + r)]

        for slice_index in range(n_slices - 1):
            for circ_index in range(n_theta):  # not -1 as it is circular
                i = shell_index * points_per_shell + n_theta * slice_index + circ_index + 1
                i_1 = shell_index * points_per_shell + n_theta * slice_index + (
                            circ_index + 1) % n_theta + 1
                # per 'cube' we need 5 tetra cells
                connectivity += [(i, i_1, i + r, i + l),
                                 (i_1, i_1 + r, i + r, i_1 + l + r),
                                 (i + r, i + l, i + r + l, i_1 + r + l),
                                 (i_1, i_1 + l, i_1 + r + l, i + l),
                                 (i_1, i + l, i + r, i_1 + r + l)]
    connectivity = np.array(connectivity)
    return connectivity