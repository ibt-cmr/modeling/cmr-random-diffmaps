""" Functionality to generate 3-dimensional, spatially smooth diffusion tensors on a LV-mesh"""
__all__ = ["generate_tensors", "generate_e2a", ]

from typing import Tuple
from copy import deepcopy

import numpy as np
import tensorflow as tf
import cmr_interpolation


def generate_tensors(eigen_value_pool: np.ndarray, flat_eigen_basis: np.ndarray,
                     flat_cylinder_coordinates: np.ndarray,
                     seed_fraction=0.1, distance_weights=(1., 1.5, 15.), kernel_variance=0.01):
    """ Function to generate an interpolated tensor "map" on a 3D LV mesh (or a slice of it)
    from a given pool of eigenvalue samples, and HA-mapping and a E2A mapping.

    :param eigen_value_pool: np.ndarray - (-1, 3) must contain enough rows to sample from
    :param flat_eigen_basis: (-1, 3, 3) Local basis of each mesh node (rad, circ, long) - vector
    :param flat_cylinder_coordinates: (-1, 3) Radial, circumferential and longitudinal coordinate
                                              of each mesh node
    :param seed_fraction: float - fraction of all nodes used as interpolation seeds
    :param distance_weights: (float, float, float) - weight of rad/circ/long distance
                              during interpolation
    :param kernel_variance: float - width of rfb-kernel used during interpolation
    :return:
    """

    # Pick random points from mask as interpolation reference points
    flat_cylinder_coordinates = flat_cylinder_coordinates + np.array([1.1, 0., 0.]).reshape(-1, 3)

    n_seeds = int(flat_cylinder_coordinates.shape[0] * seed_fraction)
    random_seed_indices = np.random.choice(np.array(range(flat_eigen_basis.shape[0])),
                                           replace=False, size=n_seeds)

    random_ref_eigenbasis = flat_eigen_basis[random_seed_indices]
    random_ref_coords = flat_cylinder_coordinates[random_seed_indices]

    # Get n_seeds eigen-value triplets and scale the eigen-basis sorted in descending order
    random_indices = np.random.choice(np.array(range(eigen_value_pool.shape[0])), size=n_seeds,
                                      replace=False)
    ev_sample = tf.sort(tf.gather(eigen_value_pool, random_indices), axis=-1)[..., tf.newaxis, ::-1]
    diagonal = tf.eye(3, 3, batch_shape=random_ref_eigenbasis.shape[0:1]) * ev_sample
    random_ref_diffusion_tensors = tf.einsum('xij, xjm, xmn -> xin', random_ref_eigenbasis,
                                             diagonal,
                                             tf.transpose(random_ref_eigenbasis, [0, 2, 1]))

    # Interpolate in log-Euclidean space
    weighted_distance_fnc = cmr_interpolation.distances.get_dist(
                                    cmr_interpolation.distances.cylindrical_distance,
                                    weights=distance_weights)
    kernel = cmr_interpolation.kernels.get_kernel(cmr_interpolation.kernels.rbf_interpolation_kernel,
                                                  kernel_variance=kernel_variance)
    tensors = cmr_interpolation.interp.interpolate_tensor(random_ref_diffusion_tensors,
                                                          random_ref_coords,
                                                          flat_cylinder_coordinates,
                                                          interpolation_kernel=kernel,
                                                          distance_metric=weighted_distance_fnc)
    return tensors


def generate_e2a(cylinder_coordinates: np.array, down_sampling_factor: float = 16.,
                 seed_fraction: float = 0.1,
                 kernel_width: float = 0.045, prob_high: float = 0.55,
                 high_angle_value: float = 90., low_angle_value: float = 5.,
                 distance_weights: Tuple[float, float, float] = (1., 5., 65.),
                 verbose: bool = False) -> tf.Tensor:
    """ Generates a random sheetlet angle map on a 3D mesh.

    :param cylinder_coordinates: (-1, 3)
    :param down_sampling_factor: (float) Determines the 'blob'-size
                                        (coarser map structure for higher down-sampling factors)
    :param seed_fraction: (float) Determines granularity/smoothness
                                    (smoother towards mean for higher fraction)
    :param kernel_width: (float) Determines smoothness together with 'seed_fraction' (higher kernel)
    :param prob_high: (float) From interval [0, 1], determines the ration high/low angles
    :param high_angle_value: (float) Value for a high angle in degree
    :param low_angle_value: (float) Value for a low angle in degree
    :param distance_weights: Tuple(float, float, float) weights for
                            (radial, circumferential, longitudinal) distance for Interpolation
    :param verbose: (bool) If True shown interpolation progress as text output
    """
    cylinder_coordinates = deepcopy(cylinder_coordinates)
    cylinder_coordinates += np.array([1.1, 0., 0.]).reshape(1, 3)
    low_res_indice = np.random.choice(range(0, cylinder_coordinates.shape[0]),
                                      int(cylinder_coordinates.shape[0] / down_sampling_factor),
                                      replace=False)
    low_res_coords = cylinder_coordinates[low_res_indice]

    n_seeds = int(low_res_coords.shape[0] * seed_fraction)
    reference_indice = np.random.choice(range(0, low_res_coords.shape[0]), size=n_seeds,
                                        replace=False)
    reference_values = tf.constant(np.random.binomial(1, prob_high, size=(n_seeds,)) *
                                   (high_angle_value - low_angle_value) + low_angle_value,
                                   dtype=tf.float32)

    random_ref_coords_lowres = low_res_coords[reference_indice]

    weighted_distance_fnc = cmr_interpolation.distances.get_dist(
                                    cmr_interpolation.distances.cylindrical_distance,
                                    weights=distance_weights)
    kernel1 = cmr_interpolation.kernels.get_kernel(cmr_interpolation.kernels.nearest_neighbour_kernel)
    interpolated_values_low_res = cmr_interpolation.interp.interpolate_scalar(
                                        reference_values,
                                        random_ref_coords_lowres,
                                        low_res_coords,
                                        distance_metric=weighted_distance_fnc,
                                        interpolation_kernel=kernel1,
                                        verbose=verbose)

    kernel2 = cmr_interpolation.kernels.get_kernel(cmr_interpolation.kernels.rbf_interpolation_kernel,
                                                   kernel_variance=kernel_width)
    interpolated_values_high_res = cmr_interpolation.interp.interpolate_scalar(
                                        interpolated_values_low_res,
                                        low_res_coords,
                                        cylinder_coordinates,
                                        distance_metric=weighted_distance_fnc,
                                        interpolation_kernel=kernel2,
                                        verbose=verbose)
    interpolated_values_high_res = np.deg2rad(interpolated_values_high_res)
    return interpolated_values_high_res


def generate_ha(transmural_position: np.ndarray,
                angle_range: Tuple[float, float] = (65., -65.)) -> np.ndarray:
    """ Generates transmurally linearly varying helix angles.

    :param transmural_position: (-1, ) - transmural position per mesh point
    :param angle_range: Tuple(float, float) - (Endo-, Epi-) Border values for helix angle
    :return: helix angle value per mesh point
    """
    scale = angle_range[1] - angle_range[0]
    helix_angles = (transmural_position * scale + angle_range[0])
    return np.deg2rad(helix_angles)


def compute_local_basis(positional_vectors: np.ndarray, mesh_numbers: Tuple[int, int, int]) -> dict:
    """ Computes the local coordinate system (radial, circumferential, longitudinal) for
    a left ventricle mesh.

    Assumes the mesh indexing to correspond to:
         without apex: (#n_shells, #n_slices, #n_theta, 3).reshape(#n_shells, -1, 3)
         concat apex: (#n_shells, 1+(#n_slices * #n_theta), 3).reshape(-1, 3)

    :param positional_vectors: (-1, 3)
    :param mesh_numbers: (#shells, #slices, #points_per_ring)
    :return: dict containing the local basis vectors per point with keys (e_r, e_c, e_l)
    """
    points_per_shell = 1 + mesh_numbers[1] * mesh_numbers[2]

    # Compute circ basis vector as difference
    mesh_vecs = positional_vectors.reshape(mesh_numbers[0], points_per_shell, 3, order="C")[:, 1:]
    mesh_vecs = mesh_vecs.reshape(mesh_numbers[0], mesh_numbers[1], mesh_numbers[2], 3, order="C")
    mesh_vecs_aug = np.concatenate([mesh_vecs, mesh_vecs[:, :, 0:1]], axis=2)

    e_c = np.diff(mesh_vecs_aug, axis=2).reshape(mesh_numbers[0], -1, 3)
    e_c = np.concatenate([e_c, np.tile(np.array([[[0., 1., 0.]]]), [mesh_numbers[0], 1, 1])],
                         axis=1).reshape(-1, 3)
    e_c /= np.linalg.norm(e_c, axis=-1, keepdims=True)

    # Compute longitudinal as difference vector between slices
    mesh_vecs = positional_vectors.reshape(mesh_numbers[0], points_per_shell, 3, order="C")[:, 1:]
    mesh_vecs = mesh_vecs.reshape(mesh_numbers[0], mesh_numbers[1], mesh_numbers[2], 3, order="C")
    mesh_vecs = np.concatenate([mesh_vecs, np.tile(positional_vectors[0:1, :].reshape(1, 1, 1, 3),
                                                   [mesh_numbers[0], 1, mesh_numbers[2], 1])],
                               axis=1)
    e_l = np.gradient(mesh_vecs, axis=1)
    e_l = np.concatenate([e_l[:, 0:1, 0], e_l[:, 1:].reshape(mesh_numbers[0], -1, 3)], axis=1)
    e_l = e_l.reshape(-1, 3)
    norm_l = np.linalg.norm(e_l, axis=-1, keepdims=True)
    e_l = np.divide(e_l, norm_l, where=norm_l > 0)

    # Compute radial vector as cross product
    e_r = np.cross(e_c, e_l)
    norm_r = np.linalg.norm(e_r, axis=-1, keepdims=True)

    e_r = np.divide(e_r, norm_r, where=norm_r > 0)

    return_dict = dict()
    return_dict["e_r"] = e_r
    return_dict["e_l"] = e_l
    return_dict["e_c"] = e_c
    return return_dict


def create_eigenbasis(local_basis: np.ndarray, helix_angles: np.ndarray,
                      e2_angles: np.ndarray) -> np.ndarray:
    """ Calculates the local diffusion tensor eigen-basis from helix angles and
    sheetlet angles, given the local coordinate system (radial, circumferential,
    longitudinal).

    :param local_basis: (-1, 3, 3) radial, circ, long vector
                              (index dimension -> -1) per mesh point
    :param helix_angles: (-1, ) Helix angle per mesh point  in radians
    :param e2_angles: (-1, ) Sheetlet angle per mesh point in radians
    :return: local eigen basis (-1, 3, 3) eigen vectors are index in -1 axis
    """

    local_rad_bv, local_circ_bv, local_long_bv = np.transpose(local_basis, [2, 0, 1])
    sin_ha = tf.math.sin(helix_angles.astype(np.float32))[:, tf.newaxis]
    cos_ha = tf.math.cos(helix_angles.astype(np.float32))[:, tf.newaxis]

    # Assign Eigen-vectors
    ev1 = cos_ha * local_circ_bv + sin_ha * local_long_bv
    ev2 = -sin_ha * local_circ_bv + cos_ha * local_long_bv
    ev3 = local_rad_bv

    # Adjust Sheetlet angle
    sin_sa = tf.math.sin(e2_angles.astype(np.float32))[:, tf.newaxis]
    cos_sa = tf.math.cos(e2_angles.astype(np.float32))[:, tf.newaxis]
    temp2, temp3 = tf.constant(ev2), tf.constant(ev3)
    ev2 = - sin_sa * temp3 + cos_sa * temp2
    ev3 = cos_sa * temp3 + sin_sa * temp2

    norms = [tf.linalg.norm(ev, axis=-1, keepdims=True) for ev in (ev1, ev2, ev3)]
    eigen_vectors = [np.divide(ev, n, where=n > 0) for ev, n in zip((ev1, ev2, ev3), norms)]
    for ev in eigen_vectors:
        ev[np.where(np.logical_not(np.isfinite(ev)))] = 0

    local_eigen_basis = tf.stack(eigen_vectors, axis=-1)
    return local_eigen_basis.numpy()

