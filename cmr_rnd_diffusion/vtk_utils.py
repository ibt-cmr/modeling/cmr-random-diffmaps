"""Functionality taken from Dr. Buoso Stefano (2020) email: buoso@biomed.ee.ethz to load LV-mesh"""

import os
from typing import List
import numpy as np
from tqdm import tqdm

from vtk import vtkUnstructuredGridReader, vtkXMLUnstructuredGridReader
from vtk import vtkUnstructuredGridWriter
from vtk import vtkPoints, vtkTetra, vtkCellArray, vtkUnstructuredGrid

from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk


def getTimeSeriesFiles(folder):
    """ Gets vtk or vtu files in the input folder
    Input:
        - folder : folder path
    Output:
        - list of the vtk/vtu files in the folder
    Dr Stefano Buoso (2021)
    buoso@biomed.ee.ethz.ch
    """

    frames_0 = np.sort(next(os.walk(folder))[2])
    frames = []
    for frame_sel in tqdm(frames_0, desc="Read Time series Files"):
        if frame_sel[0] != '.':
            if frame_sel[-3:] in ['vtu', 'vtk']:
                frames.append(frame_sel)

    return frames


def getTimeSeriesData(folder: str, frames: List[str]):
    """ Read series of UnstructuredGrid files from vtk (.vtu or .vtk)
    Input:
        - folder: path to the files
        - frames: ordered list of frames to be read
    Output:
        - initial_coordinates : Nx3 array with initial coordinates of mesh points
        - displ_field : MxNx3 array with displacement fields of mesh points at all times
                        first index selects the time frame, then an Nx3 array is the displacement
                        vector for each point
    Dr Stefano Buoso (2021)
    buoso@biomed.ee.ethz.ch
    """
    if frames[0][-3:] == 'vtk':
        reader = vtkUnstructuredGridReader()
        reader.ReadAllScalarsOn()
        reader.ReadAllVectorsOn()
    elif frames[0][-3:] == 'vtu':
        reader = vtkXMLUnstructuredGridReader()

    reader.SetFileName(folder + '/' + frames[0])
    reader.Update()
    ref_mesh = reader.GetOutput()

    initial_coordinates = vtk_to_numpy(ref_mesh.GetPoints().GetData())

    displ_field = np.zeros((len(frames), initial_coordinates.shape[0], 3))
    for count, frame_sel in tqdm(enumerate(frames), desc="Load time series data", total=len(frames)):
        reader.SetFileName(folder + '/' + frame_sel)
        reader.Update()
        displ_field[count, :, :] = vtk_to_numpy(
            reader.GetOutput().GetPointData().GetArray(0)).reshape(-1, 3)

    return initial_coordinates, displ_field


def createVTKObject(points, elements):
    """
    Create vtkUnstructuredGrid object in python
    Input:
        - points: Nx3 array where N is the number of points in 3D
        - elements: Mx4 array where M is the number of tetrahedrons
    Output:
        - vtkUnstructuredGrid
    Dr Stefano Buoso (2021)
    buoso@biomed.ee.ethz.ch
    """

    points_vtk = vtkPoints()
    tetra = vtkTetra()
    vtk_mesh = vtkUnstructuredGrid()

    for i in range(points.shape[0]):
        points_vtk.InsertNextPoint((points[i, 0], points[i, 1], points[i, 2]))

    vtk_mesh.SetPoints(points_vtk)

    cells = vtk.vtkCellArray()
    for n_el in range(elements.shape[0]):
        for i in range(4):
            tetra.GetPointIds().SetId(i, elements[n_el, i])
        vtk_mesh.InsertNextCell(tetra.GetCellType(), tetra.GetPointIds())

    return vtk_mesh


def appendScalar(vtk_mesh, scalar_vector, scalar_name):
    """ Append scalar values to vtk mesh points
    Input:
        - vtk_mesh : vtkUnstructuredGrid object
        - scalar_vector: vector of scalar values for each node, ordered as nodes indices
        - scalar_name : name of the scalars for visualization
    Output:
        - vtkUnstructuredGrid object with appended scalar value
    Dr Stefano Buoso (2021)
    buoso@biomed.ee.ethz.ch
    """
    linear_array = np.array(scalar_vector).reshape(-1, 1, order="F")
    linear_array_vtk = numpy_to_vtk(linear_array)
    linear_array_vtk.SetName(scalar_name)
    vtk_mesh.GetPointData().AddArray(linear_array_vtk)
    vtk_mesh.Modified()

    return vtk_mesh


def appendVector(vtk_mesh, vector_values, vector_name):
    """ Append vector values to vtk mesh points
    Input:
        - vtk_mesh : vtkUnstructuredGrid object
        - vector_value: NxD vector values where N is the number of nodes and D the number
                        of components
        - vector_name : name of the vector for visualization
    Output:
        - vtkUnstructuredGrid object with appended vector of values
    Dr Stefano Buoso (2021)
    buoso@biomed.ee.ethz.ch
    """
    linear_array = np.array(vector_values).reshape(-1, 1, order="F")
    linear_array_vtk = numpy_to_vtk(linear_array)
    linear_array_vtk.SetName(vector_name)
    linear_array_vtk.SetNumberOfComponents(vector_values.shape[1])
    vtk_mesh.GetPointData().AddArray(linear_array_vtk)
    vtk_mesh.Modified()

    return vtk_mesh


def saveVTKasASCII(vtk_mesh, output_file):
    """Save VTK unstructured grid as an outout ASCII file
    Input :
        - vtk_mesh : vtkUnstructuredGrid variable
        - output_file : path and name of the output file
    Output:
        - None : a vtk file is stored at the required location

    Dr Stefano Buoso (2021)
    buoso@biomed.ee.ethz.ch
    """
    writer = vtkUnstructuredGridWriter()
    writer.SetInputData(vtk_mesh)
    writer.SetFileTypeToASCII()
    writer.SetFileName(output_file)
    writer.Write()
    return 0
